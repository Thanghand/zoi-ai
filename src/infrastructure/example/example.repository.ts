import { Provider } from '@nestjs/common';
import { EXAMPLE_REPOSITORY, Example, IExampleRepository } from '../../domains';

export class ExampleLocalRepository implements IExampleRepository {
  create(domain: Example): Promise<Example> {
    throw new Error('Method not implemented.');
  }
  update(domain: Example): Promise<Example> {
    throw new Error('Method not implemented.');
  }
  deleteById(id: string): Promise<void> {
    throw new Error('Method not implemented.');
  }
  getById(id: string): Promise<Example> {
    throw new Error('Method not implemented.');
  }
}

export const exampleLocalRepositoryProvider: Provider = {
  provide: EXAMPLE_REPOSITORY,
  useValue: new ExampleLocalRepository(),
};
