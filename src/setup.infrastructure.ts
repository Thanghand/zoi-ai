import { exampleLocalRepositoryProvider } from './infrastructure/example';

export const repositories = [exampleLocalRepositoryProvider];
