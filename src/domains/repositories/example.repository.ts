import { Example } from '../models';

export const EXAMPLE_REPOSITORY = 'EXAMPLE_REPOSITORY';
export interface IExampleRepository {
  create(domain: Example): Promise<Example>;
  update(domain: Example): Promise<Example>;
  deleteById(id: string): Promise<void>;
  getById(id: string): Promise<Example>;
}
